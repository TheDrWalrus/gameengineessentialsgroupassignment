﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class P2_AI : MonoBehaviour
{
    Color mainColor;
    public bool isRed = true;
    public GameObject ball;
    public GameObject altTarget;
    NavMeshAgent agent;

    Transform hitPoint;
    public float hitRadius = 3F;
    public bool grounded = true;
    public bool canHit = true;
    public int ballUpperBound = 0;
    public int ballLowerBound = 0;
    public float zPosBoundMax = 29f;
    public float zPosBoundMin = 1f;

    public GameObject goalMid;

    Rigidbody rb;
    Rigidbody ballrb;
    ParticleSystem ps;

    public AudioClip hitClip;
    public AudioSource aiSource;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.destination = ball.transform.position;
        agent.updateRotation = false;

        hitPoint = transform.GetChild(1);

        rb = GetComponent<Rigidbody>();
        ballrb = ball.GetComponent<Rigidbody>();

        mainColor = GetComponent<Renderer>().material.color;

        ps = ball.transform.GetChild(2).GetComponent<ParticleSystem>();
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.layer == 11 && !grounded) {
            grounded = true;
            rb.isKinematic = true;
            agent.enabled = true;
        } else if (collision.gameObject.layer == 10) {
            rb.velocity = Vector3.zero;
        }
    }

    void FixedUpdate()
    {
        if (grounded) {
            if (ball.transform.position.z >= ballLowerBound && ball.transform.position.z <= ballUpperBound) {
                Vector3 target = new Vector3(ball.transform.position.x, 2, ball.transform.position.z);
                agent.destination = target;
            } else {
                agent.destination = altTarget.transform.position;
            }

            if (canHit && ball.transform.position.y > hitRadius + hitPoint.position.y && Vector3.Distance(hitPoint.position, ball.transform.position) < hitRadius * 2.5) {
                grounded = false;
                agent.enabled = false;
                rb.isKinematic = false;
                rb.AddForce(Vector3.Scale(ball.transform.position - transform.position, new Vector3(0, 1.7F, 0)) * 3, ForceMode.Impulse);
            }
        } else {
            Vector3 moveTarget = new Vector3(ball.transform.position.x, transform.position.y, ball.transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, moveTarget, 20 * Time.deltaTime);
        }

        if (Vector3.Distance(hitPoint.position, ball.transform.position) < hitRadius && canHit) { 
            Vector3 lookPoint = goalMid.transform.position + new Vector3(Random.Range(-40.0f, 40.0f), Random.Range(-20.0f, 20.0f), Random.Range(-10.0f, 10.0f));
            transform.LookAt(lookPoint);
            
            ball.GetComponent<movement>().hitBall(transform.forward);
            canHit = false;
            ball.GetComponent<movement>().setColor(mainColor, isRed);

            aiSource.clip = hitClip;
            aiSource.Play();

            ParticleSystem.MainModule main = ps.main;
            main.startColor = mainColor;
            ps.Play();
            
            StartCoroutine(timerHit());
        }

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -14f, 14f), transform.position.y, Mathf.Clamp(transform.position.z, zPosBoundMin, zPosBoundMax));
    }

    IEnumerator timerHit() {
        yield return new WaitForSeconds(0.5F);
        canHit = true;
    }
}
