﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class m_hitBall : NetworkBehaviour
{
    [SyncVar]
    public bool isRed = false;
    Color mainColor;
    public GameObject ball;
    public float hitRadius = 3;

    Rigidbody rb;
    Transform cam;
    Transform hitPoint;
    ParticleSystem ps;

    private NetworkIdentity objNetId;

    public AudioClip hitClip;
    public AudioSource playerSource;

    void Start()
    {
        
        ball = GameObject.FindGameObjectsWithTag("Ball")[0];
        cam = this.transform.GetChild(0);
        rb = ball.GetComponent<Rigidbody>();
        hitPoint = cam.GetChild(0);
        ps = ball.transform.GetChild(2).GetComponent<ParticleSystem>();

        //Set colour based on team var in m_movePlayer script
        if(GetComponent<m_movePlayer>().team == "red")
        {
            mainColor = Color.red;
            isRed = true;
        }
        else
        {
            mainColor = GetComponent<Renderer>().material.color;
        }
        if (!isLocalPlayer)
        {
            cam.gameObject.SetActive(false);
        }

    }
    
    

    // Update is called once per frame
    void Update()
    {
        //Checks if the object that this script is running on, is the local player on the clients machine
        //basically, this script will run for the object the client can control and not all of them
        if (!isLocalPlayer)
        {
            return;
        }
        
        //Check for client's mouse input
        if (Input.GetMouseButtonDown(0)) {
            if (Vector3.Distance(hitPoint.position, ball.transform.position) < hitRadius) {
                CmdCallRpcFuncs(cam.transform.forward,mainColor,isRed);
            }
        }
    }

    [Command]
    //Command sent to server must pass data of direction and colour
    //If you do not direction of ball movement and colour are collected from the server side objects
    //and not from the client's direction/colour
    void CmdCallRpcFuncs(Vector3 direction, Color newColor, bool isRed) {
        objNetId = ball.GetComponent<NetworkIdentity>();        // get the object's network ID
        ball.GetComponent<m_movement>().RpcHitBall(direction);
        ball.GetComponent<m_movement>().RpcSetColor(newColor, isRed);

        //Update on server side
        ParticleSystem.MainModule main = ps.main;
        main.startColor = mainColor;
        ps.Play();

        //Update on client side
        RpcUpdatePS(newColor);
        RpcPlaySound();
    }

    [ClientRpc]
    //Colours must be updated both on server and all clients
    void RpcUpdatePS(Color newColor)
    {
        ParticleSystem.MainModule main = ps.main;
        main.startColor = newColor;
        ps.Play();
    }
    [ClientRpc]
    void RpcPlaySound()
    {
        playerSource.clip = hitClip;
        playerSource.Play();
    }
}
