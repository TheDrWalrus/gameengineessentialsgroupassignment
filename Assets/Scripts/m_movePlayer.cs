﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class m_movePlayer : NetworkBehaviour
{
    CharacterController characterController;

    public float sprintMulti = 2.0f;
    public float speed = 10.0f;
    public float jumpStrength = 100.0f;
    public bool isGrounded = true;

    Rigidbody rb;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 11 && !isGrounded){
            isGrounded = true;
        }
    }

    //Variables for clamping
    float minZ = 0;
    float maxZ = 0;

    //variable used to determine team
    [SyncVar]
    public string team = "red";

    void Start()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        
        //Determine which character this object is (blue or red)
        //if z < -1, then blue else red
        if(transform.position.z < -1)
        {
            //blue
            team = "blue";
            //Set clamp values
            minZ = -29f;
            maxZ = -1f;
        }
        else
        {
            //red
            team = "red";
            //Update direction the player is facing
            CmdRotPlayer(Quaternion.Euler(0, 180, 0));

            //Set clamp values
            minZ = 1f;
            maxZ = 29f;

            //Change colour
            //Update to server
            CmdUpdateColour(Color.red);
        }
    }

    void Update()
    {
        if (!isLocalPlayer) {
            return;
        }
        
        float translation = Input.GetAxis("Vertical") * speed;
        float straffe = Input.GetAxis("Horizontal") * speed;
        
        if (Input.GetKey(KeyCode.LeftShift)) {
            translation *= sprintMulti;
            straffe *= sprintMulti;
            
        }

        translation *= Time.deltaTime;
        straffe *= Time.deltaTime;
 
        transform.Translate(Vector3.right * straffe);
        transform.Translate(Vector3.forward * translation);

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -14f, 14f), transform.position.y, Mathf.Clamp(transform.position.z, minZ, maxZ));

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded){
            rb.AddForce(0, jumpStrength, 0, ForceMode.Impulse);
            isGrounded = false;
        }

        if (Input.GetKeyDown("escape")) {
            Cursor.lockState = CursorLockMode.None;
        }
    }


    [Command]
    void CmdUpdateColour(Color c)
    {
        //Update player material colour
        var renderer = gameObject.GetComponent<Renderer>();
        renderer.material.color = c;
        //Update spotlight colour
        gameObject.GetComponentInChildren<Light>().color = c;
        //Update to all clients
        RpcUpdateColour(c);
    }

    [ClientRpc]
    void RpcUpdateColour(Color c)
    {
        //Update player material colour
        var renderer = gameObject.GetComponent<Renderer>();
        renderer.material.color = c;
        //Update spotlight colour
        gameObject.GetComponentInChildren<Light>().color = c;
    }

    [Command]
    void CmdRotPlayer(Quaternion rot) {
        RpcRotPlayer(rot);
    }

    [ClientRpc]
    void RpcRotPlayer(Quaternion rot) {
        transform.localRotation = rot;
    }

}