﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public AudioClip selectClip;
    public AudioClip deselectClip;
    public AudioSource menuSource;
    // Start is called before the first frame update
    public void LoadScene(string sceneName){
        menuSource.clip = selectClip;
        menuSource.Play();
        Debug.Log("Loading Scene: " + sceneName);
        SceneManager.LoadScene(sceneName);
    }

    public void LoadMultiplayerMenu() {
        menuSource.clip = selectClip;
        menuSource.Play();
        SceneManager.LoadScene("Multiplayer");
    }

    public void QuitGame() {
        menuSource.clip = deselectClip;
        menuSource.Play();
        Application.Quit();
    }
}
