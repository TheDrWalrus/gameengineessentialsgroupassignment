﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public string mainMenuScene;
    public GameObject pauseMenu;
    public bool isPaused;
    public GameObject Player;
    GameObject playerCam;
    public GameObject elevatorMusic;
    public AudioClip selectClip;
    public AudioClip deselectClip;
    public AudioSource selectSource;
    // Start is called before the first frame update
    void Start()
    {
        playerCam = Player.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            if (isPaused) {
                ResumeGame();
            } else {
                isPaused = true;
                pauseMenu.SetActive(true);
                Time.timeScale = 0f;
                elevatorMusic.GetComponent<pauseControll>().StartPlay();
                Player.GetComponent<hitBall>().enabled = false;
                playerCam.GetComponent<PlayerLook>().enabled = false;
            }
        }
    }

    public void ResumeGame() {
        isPaused = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;

        elevatorMusic.GetComponent<pauseControll>().PausePlay();
        Player.GetComponent<hitBall>().enabled = true;
        playerCam.GetComponent<PlayerLook>().enabled = true;

        selectSource.clip = selectClip;
        selectSource.Play();

        Cursor.lockState = CursorLockMode.Locked;
    }

    public void ReturnToMain() {
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainMenuScene);

        selectSource.clip = deselectClip;
        selectSource.Play();
    }
}